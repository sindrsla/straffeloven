# Straffeloven 2005 - se [lovdata](https://lovdata.no/dokument/NL/lov/2005-05-20-28)

 * Loven kan anvendes f.eks ved:
    * Innbrudd i datasystemer (§ 204)
        * Med bot eller fengsel inntil 2 år straffes den som ved å bryte en beskyttelse
        * eller ved annen uberettiget fremgangsmåte skaffer seg tilgang til datasystem eller del av det.
    * Fare for driftshindring (§ 206)
        * Med bot eller fengsel inntil 2 år straffes den som ved å overføre, skade, slette, forringe, endre,
          tilføye eller fjerne informasjon
        * uberettiget volder fare for avbrudd eller vesentlig hindring av driften av et datasystem.
    * Uberretiget befatning med tilgangsdata, dataprogram (§ 201)
        * Med bot eller fengsel inntil 1 år straffes den som med forsett om å begå en straffbar handling 
          uberettiget fremstiller, anskaffer, besitter eller gjør tilgjengelig for en annen
        * a)passord eller andre opplysninger som kan gi tilgang til databasert informasjon eller datasystem, eller
        * b)dataprogram eller annet som er særlig egnet som middel til å begå straffbare handlinger som retter seg 
          mot databasert informasjon eller datasystem. 
        * På samme måte straffes den som uten forsett om å begå en straffbar handling besitter et selvspredende dataprogram, 
          og besittelsen skyldes uberettiget fremstilling eller anskaffelse av programmet.
